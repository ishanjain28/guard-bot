'use strict';

const { compose } = require('telegraf');

const config = require('../config');
const names = config.dynamicCmds || {};

const dynamicCmds = names.map(name => `./${name}`).map(require);

module.exports = compose(dynamicCmds);
