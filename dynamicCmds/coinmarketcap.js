'use strict';

let unirest = require('unirest');
let format = require('format-number')();
let listings = {"data": []};

function reloadListings() {
    unirest.get("https://api.coinmarketcap.com/v2/listings").end(response => {
        if (response.status != 200) {
            console.error(`error occurred in updating listings ${response.status}`);
        } else {
            console.log("Updated Listings");
            listings = response.body;
            listings.data.forEach(v => {
                dynamicCommands[v.symbol] = v.website_slug;
            });
        }
    });
    // Reload every 2 hours
    setTimeout(reloadListings, 12000 * 1000);
}
reloadListings();

let dynamicCommands = {
};

function sendRequest(url) {
    return new Promise((onResolve, onReject) => {
        unirest.get(url).end(response => {
            if (response.status != 200) {
                onReject(error);
            } else {
                onResolve(response.body);
            }
        });
    });
}

function findTickerFor(website_slug) {
    let listing = listings.data.find(item => item.website_slug == website_slug);
    if (listing) {
        return new Promise((onResolve, onReject) => {
            let message = `${listing.symbol.toUpperCase()}: `;
            sendRequest(`https://api.coinmarketcap.com/v2/ticker/${listing.id}/?convert=BTC`)
                .then(resolve => {
                    message += `$${format(resolve["data"]["quotes"]["USD"]["price"])} | ${resolve["data"]["quotes"]["USD"]["percent_change_24h"]}%`;
                    message += "\n";
                    message += `${format(resolve["data"]["quotes"]["BTC"]["price"])} BTC | ${resolve["data"]["quotes"]["BTC"]["percent_change_24h"]}%`;
                    message += "\n";
                    let volume_24h = resolve["data"]["quotes"]["USD"]["volume_24h"];
                    onResolve([message, volume_24h]);
                })
                .catch(error => {
                    onReject(error)
                })
        })
        .catch(error => {
            return Promise.reject(`error in fetching information for ${listing.name}(${listing.symbol})`);
        })
        .then(data => {
                return new Promise((onResolve, onReject) => {
                    sendRequest(`https://api.coinmarketcap.com/v2/ticker/${listing.id}/?convert=ETH`)
                    .then(resolve => {
                        data[0] += `${format(resolve["data"]["quotes"]["ETH"]["price"])} ETH | ${resolve["data"]["quotes"]["ETH"]["percent_change_24h"]}%`;
                        data[0] += "\n";
                        data[0] += `Volume: $${format(resolve["data"]["quotes"]["USD"]["volume_24h"])}`;
                        return onResolve(data[0]);
                    }, error => {
                       data[0] += `Volume: $${data[1]}`;
                       return onResolve(data[0]);
                    });
               });
        })
    }

    return Promise.reject(`This currency does not exists on CoinMarketCap.
Listings are updated every 2 hours, If this currency was added recently, Please wait for some time before retrying`);
}

const createHandler = ({ replyToPm }) => ({ message, reply }, next) => {
	console.log(message)
	if (!message) {
		return next();
	}
	const { text } = message;
	console.log(text)
	const replyOptions = { reply_to_message_id: message.message_id };

	if (!text) {
		return next();
	}

    const command = text.replace("/", "").toUpperCase();
	console.log(dynamicCommands[command])
    if(dynamicCommands[command]) {
        findTickerFor(dynamicCommands[command]).then(msg => reply(msg, replyOptions), msg => reply(msg, replyOptions));
    } else {
	    return next();
    }
};

// for embedding in another bot
module.exports = createHandler({ replyToPm: false });
