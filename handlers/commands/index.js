'use strict';

const requireDir = require('require-directory');
const R = require('ramda');
const { Router } = require('telegraf');

const routingFn = require('./routingFn');

const router = new Router(routingFn);

const { listCommands } = require("../../stores/command");


const exclude = (_, filename) => filename === 'routingFn.js';
const rename = R.toLower;

const handlers = new Map(Object.entries(requireDir(module, { exclude, rename })));

// Add all the custom commands to router handlers hashmap and then map them to
// execute runCustomCmd
(async function() {
    let cmds = await listCommands();

    for(let cmd of cmds) {
        handlers.set(cmd.name, async (ctx, next) => {
            next();
        });
    }
})();

module.exports = router;
module.exports.handlers = handlers;

