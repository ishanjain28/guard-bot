'use strict';

const config = require('../config');
const eq = require('./eq');

const masterById = /^\d+$/.test(config.master[0]);
const masterByUsername = /^@?\w+$/.test(config.master[0]);

if (!masterById && !masterByUsername) {
	throw new Error('Invalid value for `master` in config file: ' +
		config.master);
}

const isMaster = masterById
	? user => { return (config.master.indexOf(user.id) == -1 ? false : true)  }
	: user => user.username && eq.username(user.username, config.master);

module.exports = {
	isMaster,
}
